package sample

import kotlinx.cinterop.*
import pigpiod.*
import platform.posix.*
import kotlin.math.min

const val LCDAddr = 0x27u
const val BLEN = 1u
var gpio = 0
var i2c = 0

fun handleSignal(n: Int) {
    println("Interrput: $n")
    i2c_close(gpio, i2c.toUInt())
    pigpio_stop(gpio)
    exit(0)
}

fun write_word(data: UInt) {
    var temp = data
    if (BLEN == 1u) {
        temp = temp or 0x08u
    } else {
        temp = temp and 0xF7u
    }
    i2c_write_byte(gpio, i2c.toUInt(), temp)
}

fun send_command(command: UInt) {

    // send bit7-4.
    var buf: UInt = command and 0xF0u
    buf = buf or 0x04u
    write_word(buf)
    time_sleep(0.002)
    buf = buf and 0xFBu
    write_word(buf)

    // send bit3-0.
    buf = (command and 0x0Fu).shl(4)
    buf = buf or 0x04u
    write_word(buf)
    time_sleep(0.002)
    buf = buf and 0xFBu
    write_word(buf)
}

fun send_data(data: UInt) {
    // send bit7-4.
    var buf: UInt = data and 0xF0u
    buf = buf or 0x05u
    write_word(buf)
    time_sleep(0.002)
    buf = buf and 0xFBu
    write_word(buf)

    // send bit3-0.
    buf = (data and 0x0Fu).shl(4)
    buf = buf or 0x05u
    write_word(buf)
    time_sleep(0.002)
    buf = buf and 0xFBu
    write_word(buf)
}

fun init() {
    send_command(0x33u)
    time_sleep(0.005)
    send_command(0x32u)
    time_sleep(0.005)
    send_command(0x28u)
    time_sleep(0.005)
    send_command(0x0Cu)
    time_sleep(0.005)
    send_command(0x01u)
    i2c_write_byte(gpio, i2c.toUInt(), 0x08u)
}

fun write(x: UInt, y: UInt, data: CharArray) {
    val rx = min(15u, x)
    val ry = min(1u, y)

    val addr = 0x80u + 0x40u * ry + rx
    send_command(addr)

    for (c in data) {
        send_data(c.toByte().toUInt())
    }
}

fun main() {
    signal(SIGINT, staticCFunction(::handleSignal))

    gpio = pigpio_start(null, null)
    if (gpio < 0) {
        println("cannot start gpio")
        exit(1)
    }

    i2c = i2c_open(gpio, 1u, LCDAddr, 0u)
    if (i2c < 0) {
        println("cannot open i2c")
        pigpio_stop(gpio)
        exit(1)
    }

    init()
//    write(0u, 0u, "Greetings!".toCharArray())
//    write(0u, 1u, "From SunFounder2".toCharArray())

    memScoped {
        val dbuffer: CArrayPointer<ByteVar> = allocArray<ByteVar>(128)
        val tbuffer: CArrayPointer<ByteVar> = allocArray<ByteVar>(128)

        while (true) {
            val timeSeconds: time_t = time(null)
            val timeRef: CPointer<tm>? = localtime(cValuesOf(timeSeconds))
            strftime(dbuffer, 128u, "%Y/%m/%d (%a)", timeRef)
            strftime(tbuffer, 128u, "%H:%M:%S", timeRef)

//            println(dbuffer.toKString())
//            println(tbuffer.toKString())
            write(0u, 0u, dbuffer.toKString().toCharArray())
            write(1u, 1u, tbuffer.toKString().toCharArray())
            time_sleep(1.0)
        }
    }

}